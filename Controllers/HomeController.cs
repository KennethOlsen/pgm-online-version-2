﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGM_online.Models;

namespace PGM_online.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // Adding 3 different supervisors
            Supervisor supervisor1 = new Supervisor { Id = 1, Name = "Tom Jones", isAvailable = true, Level = "Pro" };
            Supervisor supervisor2 = new Supervisor { Id = 2, Name = "King Kong", isAvailable = false, Level = "King" };
            Supervisor supervisor3 = new Supervisor { Id = 3, Name = "Snoopy", isAvailable = true, Level = "Beginner" };
            
            // Putting them into the SupervisorGroup-list
            SupervisorGroup.AddSupervisor(supervisor1);
            SupervisorGroup.AddSupervisor(supervisor2);
            SupervisorGroup.AddSupervisor(supervisor3);

            return View("MyFirstView");

        }

        public IActionResult SupervisorConfirmation()
        {
            
            return View("AddSupervisorConfirmation");

        }

        [HttpGet]
        public IActionResult AllSupervisors()
        {

            return View(SupervisorGroup.Supervisors);
        }

        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);
                //SupervisorGroup.AddSupervisor(supervisor);

                return View("AddSupervisorConfirmation", supervisor);
            }

            else
            {
                return View("MyFirstView");
            }

            
        }


    }

}
